const sqlite3 = require('sqlite3');
const { run } = require('./runQuery');

const parseRow = row =>
  row
    .replace(/^\| */, '')
    .replace(/ *\|$/, '')
    .split(/[ \t]*\|[ \t]*/)
    .map(value => value.trim());

const parseDataRow = dataRow => parseRow(dataRow);

const parseColumnRow = columnLine =>
  parseRow(columnLine)
    .map(columnDesc => columnDesc.split(/[ \t]+/))
    .map(([name, type]) => ({
      name,
      type,
    }));

const parseTableDescription = tableDescription => {
  const relevantLines = tableDescription
    .split('\n')
    .map(line => line.trim())
    .filter(Boolean)
    .filter(line => line.match(/[a-zA-Z]/));
  const firstLine = relevantLines[0];
  const tableName = firstLine.replace(/[^0-9a-zA-Z_]/g, '');
  const secondLine = relevantLines[1];
  const columns = parseColumnRow(secondLine);
  const rows = relevantLines.slice(2).map(parseDataRow);
  return {
    tableName,
    columns,
    rows,
  };
};

const executeTableInfo = async (db, tableInfo) => {
  await run(
    db,
    `
      CREATE TABLE
        ${tableInfo.tableName}
      (${tableInfo.columns.map(({ name, type }) => `${name} ${type || 'varchar(256)'}`).join(', ')})
    `,
  );
  for (let i = 0; i < tableInfo.rows.length; i++) {
    await run(
      db,
      `
        INSERT INTO ${tableInfo.tableName}
          (${tableInfo.columns.map(({ name }) => name).join(', ')})
        VALUES
          (${tableInfo.rows[i].join(', ')})
      `,
    );
  }
};

const createDb = async (...tables) => {
  const db = new sqlite3.Database(':memory:');
  for (let i = 0; i < tables.length; i++) {
    const tableInfo = parseTableDescription(tables[i]);
    await executeTableInfo(db, tableInfo);
  }
  return db;
};

module.exports = createDb;
