const run = (db, query) =>
  new Promise((resolve, reject) =>
    db.run(query, error => {
      if (error) {
        reject(error);
      } else {
        resolve();
      }
    }),
  );

const getAll = (db, query) =>
  new Promise((resolve, reject) =>
    db.all(query, (error, rows) => {
      if (error) {
        reject(error);
      } else {
        resolve(rows);
      }
    }),
  );

module.exports = {
  run,
  getAll,
};
