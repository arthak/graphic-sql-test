const { run, getAll } = require('./runQuery');

class MockKnex {
  constructor(db) {
    this.db = db;
  }

  async raw(query, variables) {
    let processedQuery = query;
    if (variables) {
      Object.keys(variables).forEach(key => {
        const value = variables[key];
        const placeholder = new RegExp(`:${key}`, 'g');
        if (typeof value === 'string') {
          processedQuery = processedQuery.replace(placeholder, `'${value}'`);
        } else {
          processedQuery = processedQuery.replace(placeholder, value);
        }
      });
    }

    const isSelect =
      processedQuery
        .trim()
        .toLowerCase()
        .indexOf('select') === 0;
    if (isSelect) {
      const rows = await getAll(this.db, processedQuery);
      return [rows];
    }
    await run(this.db, processedQuery);
  }
}

module.exports = MockKnex;
