/* global expect, describe, it */
const { createDb, MockKnex, run, getAll } = require('graphic-sql-test');
const getUserById = require('./getUserById');

describe('getUserById', () => {
  it('Returns the name of the user', async () => {
    const db = await createDb(
      `
        +---------------------------------------------+
        |             users                           |
        +---------------------+-----------------------+
        |      id int(11)     |   name varchar(16)    |
        +---------------------+-----------------------+
        |         10          |     'Darko'           |
        +---------------------+-----------------------+
      `,
    );
    const knex = new MockKnex(db);
    const result = await getUserById(knex, 10);
    expect(result).toEqual([{ name: 'Darko' }]);
  });

  it('Returns the name of the user (no knex)', async () => {
    const db = await createDb(
      `
        +---------------------------------------------+
        |             users                           |
        +---------------------+-----------------------+
        |      id int(11)     |   name varchar(16)    |
        +---------------------+-----------------------+
        |         10          |     'Darko'           |
        +---------------------+-----------------------+
      `,
    );
    const result = await getAll(db, 'select * from users');
    expect(result).toEqual([{ id: 10, name: 'Darko' }]);
    run(db, `UPDATE users SET name='Bob' WHERE id=10`);
    const result2 = await getAll(db, 'select * from users');
    expect(result2).toEqual([{ id: 10, name: 'Bob' }]);
  });
});
