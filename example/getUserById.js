const getUserById = async (knex, id) =>
  (
    await knex.raw(
      `
        SELECT 
            name
        FROM
            users
        WHERE
            id=:id
      `,
      {
        id,
      },
    )
  )[0];

module.exports = getUserById;
